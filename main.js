// Отрирмуємо доступ до всіх вкладок і контенту вкладок
const tabsTitle = document.querySelectorAll(".tabs-title");
const tabsContents = document.querySelectorAll(".content");

function toggleContent(content) {
  tabsContents.forEach((c) => {
    if (c.dataset.content !== content) {
      c.style.display = "none";
    }
  });

  document.querySelector(`[data-content="${content}"]`).style.display = "block";
}

tabsTitle.forEach((tabs) => {
  tabs.addEventListener("click", () => {
    const targetContent = tabs.dataset.target;
    toggleContent(targetContent);
  });
});
